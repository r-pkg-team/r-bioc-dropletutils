Source: r-bioc-dropletutils
Section: gnu-r
Priority: optional
Maintainer: Debian R Packages Maintainers <r-pkg-team@alioth-lists.debian.net>
Uploaders: Steffen Moeller <moeller@debian.org>
Vcs-Browser: https://salsa.debian.org/r-pkg-team/r-bioc-dropletutils
Vcs-Git: https://salsa.debian.org/r-pkg-team/r-bioc-dropletutils.git
Homepage: https://bioconductor.org/packages/DropletUtils/
Standards-Version: 4.7.0
Rules-Requires-Root: no
Build-Depends: debhelper-compat (= 13),
               dh-r,
               r-base-dev,
               r-bioc-singlecellexperiment,
               r-cran-matrix,
               r-cran-rcpp,
               r-bioc-biocgenerics,
               r-bioc-s4vectors,
               r-bioc-iranges,
               r-bioc-genomicranges,
               r-bioc-summarizedexperiment,
               r-bioc-biocparallel,
               r-bioc-sparsearray (>= 1.5.18),
               r-bioc-delayedarray (>= 0.31.9),
               r-bioc-delayedmatrixstats,
               r-bioc-hdf5array,
               r-bioc-rhdf5,
               r-bioc-edger,
               r-cran-r.utils,
               r-cran-dqrng,
               r-bioc-beachmat,
               r-bioc-scuttle,
               r-bioc-rhdf5lib,
               r-cran-bh,
               libhdf5-dev,
               libpcg-cpp-dev,
               architecture-is-64-bit
Testsuite: autopkgtest-pkg-r

Package: r-bioc-dropletutils
Architecture: any
Depends: ${R:Depends},
         ${shlibs:Depends},
         ${misc:Depends}
Recommends: ${R:Recommends}
Suggests: ${R:Suggests}
Description: BioConductor utilities for handling single-cell droplet data
 Provides a number of utility functions for handling single-cell
 (RNA-seq) data from droplet technologies such as 10X Genomics. This
 includes data loading from count matrices or molecule information files,
 identification of cells from empty droplets, removal of barcode-swapped
 pseudo-cells, and downsampling of the count matrix.
